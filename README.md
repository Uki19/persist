# Persist

[![CI Status](https://img.shields.io/travis/Uros Zivaljevic/Persist.svg?style=flat)](https://travis-ci.org/Uros Zivaljevic/Persist)
[![Version](https://img.shields.io/cocoapods/v/Persist.svg?style=flat)](https://cocoapods.org/pods/Persist)
[![License](https://img.shields.io/cocoapods/l/Persist.svg?style=flat)](https://cocoapods.org/pods/Persist)
[![Platform](https://img.shields.io/cocoapods/p/Persist.svg?style=flat)](https://cocoapods.org/pods/Persist)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Persist is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Persist'
```

## Author

Uros Zivaljevic, uroszivaljevic19@gmail.com

## License

Persist is available under the MIT license. See the LICENSE file for more info.
