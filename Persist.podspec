#
# Be sure to run `pod lib lint Persist.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'Persist'
    s.version          = '1.0.8'
    s.summary          = 'Private persistence library on top of RealmSwift.'
    
    # This description is used to generate tags and improve search results.
    #   * Think: What does it do? Why did you write it? What is the focus?
    #   * Try to keep it short, snappy and to the point.
    #   * Write the description between the DESC delimiters below.
    #   * Finally, don't worry about the indent, CocoaPods strips it!
    
    s.description      = <<-DESC
    This project is used for simple storage layer over SwiftRealm.
    It provides simpler method for CRUD operations.
    DESC
    
    s.homepage         = 'https://bitbucket.org/Uki19/persist'
    # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'Uros Zivaljevic' => 'uroszivaljevic19@gmail.com' }
    s.source           = { :git => 'https://Uki19@bitbucket.org/Uki19/persist.git', :tag => s.version.to_s }
    s.swift_version    = '5.0'
    
    s.ios.deployment_target = '10.0'
    
    s.source_files = 'Persist/*.swift'
    
    # s.resource_bundles = {
    #   'Persist' => ['Persist/Assets/*.png']
    # }
    
    # s.public_header_files = 'Pod/Classes/**/*.h'
    # s.frameworks = 'UIKit', 'MapKit'
    s.dependency 'RealmSwift'
end
