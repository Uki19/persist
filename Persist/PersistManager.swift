//
//  PersistManager.swift
//  Persist
//
//  Created by Uros Zivaljevic on 12/6/18.
//  Copyright © 2018 Uros Zivaljevic. All rights reserved.
//

import RealmSwift
import Foundation

public struct PersistManager {
    
    public static let instance = PersistManager()
    
    private init() {
        configure()
    }
    
    private var realm: Realm {
        do {
            let realmInstance = try Realm()
            return realmInstance
        } catch {
            fatalError("Error getting Realm instance")
        }
    }

    public func configure() {
        var config = Realm.Configuration()
        config.deleteRealmIfMigrationNeeded = true
        Realm.Configuration.defaultConfiguration = config
    }

    //MARK: Storing

    func store<T: Object>(_ object: T, update: Bool) -> T {
        do {
            try realm.write {
                realm.add(object, update: update ? .all : .error)
            }
            return object
        } catch {
            fatalError("Error storing object \(object)")
        }
    }

    func store<T: Object>(_ objects: List<T>, update: Bool) -> List<T> {
        do {
            try realm.write {
                realm.add(objects, update: update ? .all : .error)
            }
            return objects
        } catch {
            fatalError("Error storing objects \(objects)")
        }
    }

    //MARK: Reading
    
    func getAll<T: Object>() -> Results<T> {
        return realm.objects(T.self)
    }
    
    func get<T: Object>(predicate: NSPredicate?, sortBy: [SortDescriptor]?) -> Results<T> {
        var results: Results<T> = getAll()
        if let predicate = predicate {
            results = results.filter(predicate)
        }
        if let sortBy = sortBy {
            results = results.sorted(by: sortBy)
        }
        return results
    }
    
    func get<T: Object, KeyType>(primaryKey: KeyType) -> T? {
        return realm.object(ofType: T.self, forPrimaryKey: primaryKey)
    }

    //MARK: Updating

    func update(_ block: (() throws -> Void)) {
        do {
            try realm.write(block)
        } catch {
            fatalError("Error updating object")
        }
    }

    //MARK: Deleting
    
    func delete<T: Object>(_ object: T) {
        do {
            try realm.write {
                realm.delete(object)
            }
        } catch {
            fatalError("Error deleting object \(object)")
        }
    }

    func deleteIfExists<T: Object>(_ object: T) {
        guard let primaryKey = T.primaryKey(),
            let primaryKeyValue = object["\(primaryKey)"] else {
            return
        }
        if let objectToDelete: T = get(primaryKey: primaryKeyValue) {
            delete(objectToDelete)
        }
    }
    
    func delete<S: Sequence>(_ objects: S) where S.Iterator.Element: Object {
        do {
            try realm.write {
                realm.delete(objects)
            }
        } catch {
            fatalError("Error deleting objects \(objects)")
        }
    }

    func delete<T: Object>(_ objects: Results<T>) {
        do {
            try realm.write {
                realm.delete(objects)
            }
        } catch {
            fatalError("Error deleting objects \(objects)")
        }
    }

    public func deleteAll() {
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch {
            fatalError("Error deleting all objects")
        }
    }
}
