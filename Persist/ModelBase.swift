//
//  ModelBase.swift
//  Persist
//
//  Created by Uros Zivaljevic on 12/6/18.
//  Copyright © 2018 Uros Zivaljevic. All rights reserved.
//

import Foundation
import RealmSwift


// MARK: Parsing

public protocol Parsable {
    static func parse(data: Data) throws -> Self
    func toJSONString() -> String?
    func toJSON() -> [String: Any]?
}

extension Parsable where Self: Codable {

    public static func parse(data: Data) throws -> Self {
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            decoder.dateDecodingStrategy = .iso8601
            let decoded: Self = try decoder.decode(Self.self, from: data)
            return decoded
        } catch {
            throw error
        }
    }

    public func toJSONString() -> String? {
        do {
            let encoder = JSONEncoder()
            encoder.keyEncodingStrategy = .convertToSnakeCase
            let encoded = try encoder.encode(self)
            return String(data: encoded, encoding: .utf8)
        } catch {
            return nil
        }
    }

    public func toJSON() -> [String: Any]? {
        do {
            let encoder = JSONEncoder()
            encoder.keyEncodingStrategy = .convertToSnakeCase
            let encoded = try encoder.encode(self)
            return try JSONSerialization.jsonObject(with: encoded, options: []) as? [String: Any]
        } catch {
            return nil
        }
    }

}

extension Array: Parsable where Element: Codable {}
extension List: Parsable where Element: Codable {}

// MARK: Storing

public protocol Storable {
    func store(update: Bool) -> Self
}

extension Storable where Self: Object {

    @discardableResult
    public func store(update: Bool = true) -> Self {
        return PersistManager.instance.store(self, update: update)
    }
}

extension List: Storable where Element: Object {

    @discardableResult
    public func store(update: Bool = true) -> List<Element> {
        return PersistManager.instance.store(self, update: update)
    }
}

// MARK: Reading

public protocol Readable {
    static func getAll<T: Object>() -> Results<T>
    static func get<T: Object>(predicate: NSPredicate?, sortBy: [SortDescriptor]?) -> Results<T>
    static func get<T: Object, KeyType>(primaryKey: KeyType) -> T?
}

extension Readable where Self: Object {

    public static func getAll<T: Object>() -> Results<T> {
        return PersistManager.instance.getAll()
    }

    public static func get<T: Object>(predicate: NSPredicate? = nil, sortBy: [SortDescriptor]? = nil) -> Results<T> {
        return PersistManager.instance.get(predicate: predicate, sortBy: sortBy)
    }

    public static func get<T: Object, KeyType>(primaryKey: KeyType) -> T? {
        return PersistManager.instance.get(primaryKey: primaryKey)
    }
}

// MARK: Updating

public protocol Updatable {
    func update(_ block: ((Self) -> Void))
}

extension Updatable where Self: Object {
    public func update(_ block: ((Self) -> Void)) {
        PersistManager.instance.update {
            block(self)
        }
    }
}

// MARK: Deleting

public enum DeletionType {
    case soft
    case none
}

public protocol Deletable {
    func delete()
}

extension Deletable where Self: Object {

    public func delete() {
        PersistManager.instance.delete(self)
    }
}

extension Results: Deletable where Element: Object {
    public func delete() {
        PersistManager.instance.delete(self)
    }
}

extension List: Deletable where Element: Object {
    public func delete() {
        PersistManager.instance.delete(self)
    }
}

// MARK: Invalidating

public protocol Invalidatable {

    var deletionType: DeletionType { get }
    var deletedAt: Date? { get set }
    func invalidate() -> Self
}

public extension Invalidatable {
    var deletionType: DeletionType {
        return .none
    }

    var deletedAt: Date? {
        get {
            return nil
        }
        set {
            print("Setting deletedAt in object that has no deletedAt property")
        }
    }
}

extension Invalidatable where Self: Object {

    @discardableResult
    public func invalidate() -> Self {
        switch deletionType {
        case .soft:
            if deletedAt != nil {
                PersistManager.instance.delete(self)
            }
        case .none:
            break
        }
        return self
    }
}

extension List: Invalidatable where Element: Object & Invalidatable {

    @discardableResult
    public func invalidate() -> List<Element> {
        let validElements = List<Element>()
        self.forEach {
            switch $0.deletionType {
            case .soft:
                if $0.deletedAt == nil {
                    validElements.append($0)
                } else {
                    PersistManager.instance.deleteIfExists($0)
                }
            case .none:
                validElements.append($0)
            }
        }
        return validElements
    }
}

public typealias ModelBase = Codable & Parsable & Storable & Invalidatable
public typealias CRUDable = Updatable & Deletable & Readable
public typealias PersistableModel = ModelBase & CRUDable
